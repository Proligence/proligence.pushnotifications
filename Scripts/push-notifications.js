﻿(function ($) {
    function isJsonString(obj) {
        if (typeof obj != "string")
            return false;
        try {
            return eval("(" + obj + ")");
        } catch (e) {
            return false;
        }
    }

    toastr.options.timeOut = 5000;
    toastr.options.debug = false;
    toastr.options.positionClass = "toast-top-full-width";
    var connection = $.connection("/conn/notification-connection");
    connection.logging = true;
    
    connection.received(function (data) {
        var msg;
        if (!(msg = isJsonString(data))) {
            msg = data;
        }
        
        msg.type = msg.type ? msg.type : 'info';

        if (msg.type !== 'js') {
            toastr[msg.type](msg.message, msg.title);
        } else {
            (new Function(msg.message))();
        }
    });
    
    connection.start({ transport: 'auto' });
}(window.jQuery));