﻿using System;
using Orchard.Localization;

namespace Proligence.PushNotifications.Services
{
    public static class PushNotifierExtensions
    {
        /// <summary>
        /// Sends a new notification of type Information
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="title">A localized message title to display</param>
        /// <param name="message">A localized message to display</param>
        public static void Information(this IPushNotifier notifier, LocalizedString title, LocalizedString message)
        {
            notifier.Send(NotifyType.Info, title, message);
        }

        /// <summary>
        /// Sends a new notification of type Success
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="title">A localized message title to display</param>
        /// <param name="message">A localized message to display</param>
        public static void Success(this IPushNotifier notifier, LocalizedString title, LocalizedString message)
        {
            notifier.Send(NotifyType.Success, title, message);
        }

        /// <summary>
        /// Sends a new notification of type Warning
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="title">A localized message title to display</param>
        /// <param name="message">A localized message to display</param>
        public static void Warning(this IPushNotifier notifier, LocalizedString title, LocalizedString message)
        {
            notifier.Send(NotifyType.Warning, title, message);
        }

        /// <summary>
        /// Sends a new notification of type Error
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="title">A localized message title to display</param>
        /// <param name="message">A localized message to display</param>
        public static void Error(this IPushNotifier notifier, LocalizedString title, LocalizedString message)
        {
            notifier.Send(NotifyType.Error, title, message);
        }

        /// <summary>
        /// Sends a new notification of type Information
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="message">A localized message to display</param>
        public static void Information(this IPushNotifier notifier, LocalizedString message)
        {
            notifier.Send(NotifyType.Info, null, message);
        }

        /// <summary>
        /// Sends a new notification of type Information
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="message">A localized message to display</param>
        public static void Success(this IPushNotifier notifier, LocalizedString message)
        {
            notifier.Send(NotifyType.Success, null, message);
        }

        /// <summary>
        /// Sends a new notification of type Warning
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="message">A localized message to display</param>
        public static void Warning(this IPushNotifier notifier, LocalizedString message)
        {
            notifier.Send(NotifyType.Warning, null, message);
        }

        /// <summary>
        /// Sends a new notification of type Error
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,LocalizedString,LocalizedString)"/>
        /// <param name="notifier"></param>
        /// <param name="message">A localized message to display</param>
        public static void Error(this IPushNotifier notifier, LocalizedString message)
        {
            notifier.Send(NotifyType.Error, null, message);
        }

        /// <summary>
        /// Executes an arbitrary JavaScript code on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="script">Script to execute.</param>
        public static void ExecuteScript(this IPushNotifier notifier, string script)
        {
            notifier.Send(NotifyType.Js, null, script);
        }

        /// <summary>
        /// Shows items that match given jQuery selector on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="selector">Client items' selector.</param>
        public static void ExecuteShow(this IPushNotifier notifier, string selector)
        {
            notifier.ExecuteScript(String.Format("$('{0}').show();", selector));
        }

        /// <summary>
        /// Hides items that match given jQuery selector on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="selector">Client items' selector.</param>
        public static void ExecuteHide(this IPushNotifier notifier, string selector)
        {
            notifier.ExecuteScript(String.Format("$('{0}').hide();", selector));
        }

        /// <summary>
        /// Adds CSS class to all items that match given jQuery selector on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="selector">Client items' selector.</param>
        /// <param name="class">CSS class to add.</param>
        public static void ExecuteAddClass(this IPushNotifier notifier, string selector, string @class)
        {
            notifier.ExecuteScript(String.Format("$('{0}').addClass({1})", selector, @class));
        }

        /// <summary>
        /// Removes CSS class from all items that match given jQuery selector on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="selector">Client items' selector.</param>
        /// <param name="class">CSS class to remove.</param>
        public static void ExecuteRemoveClass(this IPushNotifier notifier, string selector, string @class)
        {
            notifier.ExecuteScript(String.Format("$('{0}').removeClass({1})", selector, @class));
        }

        /// <summary>
        /// Toggles CSS class on all items that match given jQuery selector on the client.
        /// </summary>
        /// <seealso cref="Proligence.PushNotifications.Services.IPushNotifier.Send(NotifyType,string, string)"/>
        /// <param name="notifier"></param>
        /// <param name="selector">Client items' selector.</param>
        /// <param name="class">CSS class to toggle.</param>
        public static void ExecuteToggleClass(this IPushNotifier notifier, string selector, string @class)
        {
            notifier.ExecuteScript(String.Format("$('{0}').toggleClass({1})", selector, @class));
        }
    }
}