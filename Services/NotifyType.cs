﻿namespace Proligence.PushNotifications.Services
{
    public enum NotifyType
    {
        /// <summary>
        /// 'Success' information, ie. green box.
        /// </summary>
        Success,

        /// <summary>
        /// Info information, ie. blue box.
        /// </summary>
        Info,

        /// <summary>
        /// Warning information, ie. orange box.
        /// </summary>
        Warning,

        /// <summary>
        /// Error information, ie. red box.
        /// </summary>
        Error,

        /// <summary>
        /// Message contains arbitrary JavaScript code to execute on client.
        /// </summary>
        Js
    }
}