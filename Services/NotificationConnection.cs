﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using JetBrains.Annotations;
using Microsoft.AspNet.SignalR;
using Orchard;
using Proligence.SignalR.Core;

namespace Proligence.PushNotifications.Services
{
    [UsedImplicitly]
    [Connection("notification-connection")]
    public class NotificationConnection : PersistentConnection
    {
        private readonly INotificationConnectionHolder _holder;
        private readonly IWorkContextAccessor _accessor;

        public NotificationConnection(INotificationConnectionHolder holder, IWorkContextAccessor accessor)
        {
            _holder = holder;
            _accessor = accessor;
        }

        protected override Task OnConnected(IRequest request, string connectionId)
        {
            var userName = GetUserName(request);
            if (userName != null)
            {
                _holder.Add(userName, connectionId);    
            }

            return base.OnConnected(request, connectionId);
        }

        protected override Task OnDisconnected(IRequest request, string connectionId, bool stopCalled)
        {
            _holder.Remove(connectionId);
            return base.OnDisconnected(request, connectionId, stopCalled);
        }

        protected override Task OnReconnected(IRequest request, string connectionId)
        {
            _holder.Remove(connectionId);
            return base.OnReconnected(request, connectionId);
        }

        private string GetUserName(IRequest request)
        {
            using(var scope = _accessor.CreateWorkContextScope(GetContext(request)))
            {
                var workContext = scope.WorkContext;
                return workContext != null
                    ? workContext.CurrentUser != null
                        ? workContext.CurrentUser.UserName
                        : null
                    : null;
            }
        }

        private HttpContextBase GetContext(IRequest request)
        {
            return ((RequestContext)request.Environment["System.Web.Routing.RequestContext"]).HttpContext;
        }
    }
}