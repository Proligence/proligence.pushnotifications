﻿using Orchard;
using Orchard.Localization;

namespace Proligence.PushNotifications.Services
{
    public interface IPushNotifier : ISingletonDependency
    {
        void Send(NotifyType type, LocalizedString title, LocalizedString message);
        void Send(NotifyType type, string title, string message);
    }
}