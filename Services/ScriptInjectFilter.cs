﻿using System.Web.Mvc;
using JetBrains.Annotations;
using Orchard.Mvc.Filters;
using Orchard.UI.Admin;
using Orchard.UI.Resources;

namespace Proligence.PushNotifications.Services
{
    [UsedImplicitly]
    public class ScriptInjectFilter : FilterProvider, IResultFilter
    {
        private readonly IResourceManager _resourceManager;

        public ScriptInjectFilter(IResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // Should only run on a full view rendering result
            if (!(filterContext.Result is ViewResult))
                return;

            if (AdminFilter.IsApplied(filterContext.RequestContext))
                return;

            _resourceManager.Require("script", "PushNotifications").AtHead();
            _resourceManager.Require("stylesheet", "PushNotifications").AtHead();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}