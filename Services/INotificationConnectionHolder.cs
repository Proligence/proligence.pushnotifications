﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Orchard;

namespace Proligence.PushNotifications.Services
{
    /// <summary>
    /// Holds user-connection id mappings.
    /// </summary>
    public interface INotificationConnectionHolder : ISingletonDependency
    {
        /// <summary>
        /// Gets a list of current connection ids for a given username.
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> Get(string userName);

        void Add(string userName, string connectionId);

        string Remove(string connectionId);
    }

    public class NotificationConnectionHolder : INotificationConnectionHolder
    {
        private readonly ConcurrentDictionary<string, string> _mappings = new ConcurrentDictionary<string, string>(); 
        public IEnumerable<string> Get(string userName)
        {
            return _mappings.Where(kvp => kvp.Value == userName).Select(kvp => kvp.Key);
        }

        public void Add(string userName, string connectionId)
        {
            _mappings.AddOrUpdate(connectionId, userName, (key, value) => userName);
        }

        public string Remove(string connectionId)
        {
            string userName;
            _mappings.TryRemove(connectionId, out userName);
            return userName;
        }
    }
}