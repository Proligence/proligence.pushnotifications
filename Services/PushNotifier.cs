﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using Orchard;
using Orchard.Localization;
using Orchard.Logging;

namespace Proligence.PushNotifications.Services
{
    public class PushNotifier : IPushNotifier
    {
        private readonly IConnectionManager _connections;
        private readonly IWorkContextAccessor _accessor;
        private readonly INotificationConnectionHolder _holder;

        public PushNotifier(IConnectionManager connections, IWorkContextAccessor accessor, INotificationConnectionHolder holder)
        {
            _connections = connections;
            _accessor = accessor;
            _holder = holder;
            Logger = NullLogger.Instance;
        }

        public void Send(NotifyType type, LocalizedString title, LocalizedString message)
        {
            Send(type,
                title != null ? title.Text : null, 
                message != null ? message.Text : null);
        }

        public void Send(NotifyType type, string title, string message)
        {
            var workContext = _accessor.GetContext();
            var context = _connections.GetConnectionContext<NotificationConnection>();

            var userName = workContext != null
                ? workContext.CurrentUser != null
                    ? workContext.CurrentUser.UserName
                    : null
                : null;

            // Currently disallowing sending push notifications to anonymous users.
            // We cannot use session Id for identifying users.
            if (userName == null) return;

            var connectionIds = _holder.Get(userName);
            if (connectionIds == null) return;

            foreach (var id in connectionIds)
            {
                context.Connection.Send(id, new
                {
                    type = type.ToString().ToLowerInvariant(), 
                    title, 
                    message,
                    user = userName
                });
            }
        }

        public ILogger Logger { get; set; }
    }
}