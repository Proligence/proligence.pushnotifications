﻿using System.Web.Mvc;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.Themes;

namespace Proligence.PushNotifications.Controllers
{
    [Themed]
    public class TraceController : Controller
    {
        private readonly IContentManager _contentManager;

        public TraceController(IContentManager contentManager)
        {
            _contentManager = contentManager;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public ActionResult Index()
        {
            return View();
        }
    }
}