using JetBrains.Annotations;
using Orchard.UI.Resources;

namespace Proligence.PushNotifications {
    [UsedImplicitly]
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();
            manifest.DefineStyle("Toastr").SetUrl("toastr.min.css", "toastr.css").SetVersion("1.3");
            manifest.DefineScript("Toastr").SetUrl("toastr.min.js", "toastr.js").SetVersion("1.3").SetDependencies("jQuery");

            manifest.DefineScript("PushNotifications").SetUrl("push-notifications.min.js", "push-notifications.js").SetDependencies("jQuery", "jQuery_SignalR", "Toastr");
            manifest.DefineStyle("PushNotifications").SetUrl("push-notifications.css").SetDependencies("Toastr");
        }
    }
}
