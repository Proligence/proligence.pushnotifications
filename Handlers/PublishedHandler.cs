using System.Web.Mvc;
using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using Proligence.PushNotifications.Services;

namespace Proligence.PushNotifications.Handlers {
    [UsedImplicitly]
    public class PublishedHandler : ContentHandler {
        private readonly IPushNotifier _notifier;

        public Localizer T { get; set; }

        public PublishedHandler(IPushNotifier notifier, UrlHelper url) {
            _notifier = notifier;
            T = NullLocalizer.Instance;

            OnPublished<ContentPart>((context, part) => 
                _notifier.Information(
                    T("'<a href=\"{1}\">{0}</a>' has been updated.", 
                        part.ContentItem.ContentManager.GetItemMetadata(part.ContentItem).DisplayText, 
                        url.RouteUrl(part.ContentItem.ContentManager.GetItemMetadata(part.ContentItem).DisplayRouteValues))));
        }
    }
}